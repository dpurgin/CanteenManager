package com.example.canteenchecker.canteenmanager.remote;

public class RemoteServiceFactory {
    public static RemoteService create() {
        return create(null);
    }

    public static RemoteService create(String authToken) {
        return new HttpsRemoteService(authToken);
    }
}
