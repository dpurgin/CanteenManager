package com.example.canteenchecker.canteenmanager.util;

import java.util.Observable;

public class Event extends Observable {
    public void emit() {
        setChanged();
        notifyObservers();
    }

    public void emit(Object arg) {
        setChanged();
        notifyObservers(arg);
    }
}
