package com.example.canteenchecker.canteenmanager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.example.canteenchecker.canteenmanager.domain.Canteen;

import java.util.Locale;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Logger;

public class CanteenDetailsFragment extends Fragment {
    private static final String TAG = CanteenRatingsFragment.class.toString();
    private static final int PICK_ADDRESS = 1002;

    private MainActivity _mainActivity;
    private TextInputLayout _canteenNameWrapper;
    private TextInputLayout _canteenMealWrapper;
    private TextInputLayout _canteenPriceWrapper;
    private TextInputLayout _canteenAddressWrapper;
    private TextInputLayout _canteenWebSiteWrapper;
    private TextInputLayout _canteenPhoneWrapper;
    private NumberPicker _canteenWaitingTimePicker;
    private FloatingActionButton _fab;

    // Enable UI on event
    private Observer _uiEnabler = new Observer() {
        @Override
        public void update(Observable observable, Object o) {
            Logger.getLogger(TAG).info("enabling UI");
            enableUi(true);
        }
    };

    // Disable UI on event
    private Observer _uiDisabler = new Observer() {
        @Override
        public void update(Observable observable, Object o) {
            Logger.getLogger(TAG).info("disabling UI");
            enableUi(false);
        }
    };

    class SaveButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            Canteen canteen = _mainActivity.getCanteen();

            canteen.name = _canteenNameWrapper.getEditText().getText().toString();
            canteen.meal = _canteenMealWrapper.getEditText().getText().toString();
            canteen.mealPrice = Double.parseDouble(
                    _canteenPriceWrapper.getEditText().getText().toString());
            canteen.address = _canteenAddressWrapper.getEditText().getText().toString();
            canteen.website = _canteenWebSiteWrapper.getEditText().getText().toString();
            canteen.phone = _canteenPhoneWrapper.getEditText().getText().toString();
            canteen.averageWaitingTime = String.valueOf(_canteenWaitingTimePicker.getValue());

            _mainActivity.saveData();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _mainActivity = (MainActivity)getActivity();
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_canteen_details, container, false);

        _canteenNameWrapper = view.findViewById(R.id.canteenNameWrapper);
        _canteenMealWrapper = view.findViewById(R.id.canteenMealWrapper);
        _canteenPriceWrapper = view.findViewById(R.id.canteenPriceWrapper);
        _canteenAddressWrapper = view.findViewById(R.id.canteenAddressWrapper);
        _canteenWebSiteWrapper = view.findViewById(R.id.canteenWebSiteWrapper);
        _canteenPhoneWrapper = view.findViewById(R.id.canteenPhoneWrapper);
        _canteenWaitingTimePicker = view.findViewById(R.id.canteenWaitingTimePicker);

        _canteenWaitingTimePicker.setMinValue(0);
        _canteenWaitingTimePicker.setMaxValue(60);

        _fab = (FloatingActionButton) view.findViewById(R.id.fab);
        _fab.setOnClickListener(new SaveButtonClickListener());

        updateUi();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        _mainActivity.getSupportActionBar().setTitle(R.string.title_fragment_details);

        subscribe();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.fragment_canteen_details, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.pickAddressMenu) {
            if (_mainActivity.getCanteen() == null) {
                Toast.makeText(getContext(), R.string.canteen_not_available, Toast.LENGTH_SHORT).show();
            } else {
                Intent intent = new Intent(getContext(), MapsActivity.class);
                intent.putExtra("address", _mainActivity.getCanteen().address);

                startActivityForResult(intent, PICK_ADDRESS);
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_ADDRESS && resultCode == Activity.RESULT_OK) {
            _canteenAddressWrapper.getEditText().setText(data.getStringExtra("address"));
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPause() {
        super.onPause();
        unsubscribe();
    }

    private void enableUi(boolean enable) {
        _canteenNameWrapper.setEnabled(enable);
        _canteenMealWrapper.setEnabled(enable);
        _canteenPriceWrapper.setEnabled(enable);
        _canteenAddressWrapper.setEnabled(enable);
        _canteenWebSiteWrapper.setEnabled(enable);
        _canteenPhoneWrapper.setEnabled(enable);
        _canteenWaitingTimePicker.setEnabled(enable);
        _fab.setEnabled(enable);
    }

    private void updateUi() {
        Logger.getLogger(TAG).info("updateUi()");

        Canteen canteen = _mainActivity.getCanteen();

        if (canteen != null) {
            _canteenNameWrapper.getEditText().setText(canteen.name);
            _canteenMealWrapper.getEditText().setText(canteen.meal);
            _canteenPriceWrapper.getEditText().setText(
                    String.format(Locale.getDefault(),"%.2f", canteen.mealPrice));
            _canteenAddressWrapper.getEditText().setText(canteen.address);
            _canteenWebSiteWrapper.getEditText().setText(canteen.website);
            _canteenPhoneWrapper.getEditText().setText(canteen.phone);
            _canteenWaitingTimePicker.setValue(Integer.parseInt(canteen.averageWaitingTime));
        }
    }

    private void subscribe() {
        _mainActivity.beginDataLoadEvent.addObserver(_uiDisabler);
        _mainActivity.dataLoadedEvent.addObserver(_uiEnabler);
        _mainActivity.beginDataSaveEvent.addObserver(_uiDisabler);
        _mainActivity.dataSavedEvent.addObserver(_uiEnabler);
    }

    private void unsubscribe() {
        _mainActivity.beginDataLoadEvent.deleteObserver(_uiDisabler);
        _mainActivity.dataLoadedEvent.deleteObserver(_uiEnabler);
        _mainActivity.beginDataSaveEvent.deleteObserver(_uiDisabler);
        _mainActivity.dataSavedEvent.deleteObserver(_uiEnabler);
    }
}
