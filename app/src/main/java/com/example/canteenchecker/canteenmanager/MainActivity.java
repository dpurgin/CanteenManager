package com.example.canteenchecker.canteenmanager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.canteenchecker.canteenmanager.domain.Canteen;
import com.example.canteenchecker.canteenmanager.remote.RemoteService;
import com.example.canteenchecker.canteenmanager.remote.RemoteServiceFactory;
import com.example.canteenchecker.canteenmanager.util.Event;

import java.util.logging.Logger;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.toString();
    private static final int INTENT_REQUEST_LOGIN = 1001;
    private static final String CANTEEN_ID_KEY = "CanteenId";
    private static final String UNEXPECTED_ERROR = "Unexpected error has occurred";
    private static final String SAVED_SUCCESSFULLY = "Saved successfully!";
    private static final String CANNOT_SAVE = "Data could not be saved!";

    private NavigationView _navigationView;
    private DrawerLayout _drawerLayout;

    private TextView _canteenNameText;
    private TextView _canteenWebSiteText;

    private Canteen _canteen = null;
    private boolean _isUnauthorized = false;

    public Event beginDataLoadEvent = new Event();
    public Event beginDataSaveEvent = new Event();
    public Event dataLoadedEvent = new Event();
    public Event dataSavedEvent = new Event();

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (getCanteen() != null &
                    getCanteen().canteenId != null &&
                    getCanteen().canteenId.equals(EventBus.extractCanteenId(intent))) {
                loadData();
            }
        }
    };

    class GetCanteenTask extends AsyncTask<Void, Void, Canteen> {
        @Override
        protected Canteen doInBackground(Void... voids) {
            RemoteService service =
                    RemoteServiceFactory.create(
                            CanteenManagerApplication.getInstance().getAuthToken());

            Canteen result = service.getCanteen();
            setIsUnauthorized(service.isUnauthorized());

            return result;
        }

        @Override
        protected void onPostExecute(Canteen canteen) {
            setCanteen(canteen);

            if (getIsUnauthorized()) {
                showSignInActivity();
            } else if (canteen != null) {
                _canteenNameText.setText(canteen.name);
                _canteenWebSiteText.setText(canteen.website);
            } else {
                Toast.makeText(getApplicationContext(), UNEXPECTED_ERROR, Toast.LENGTH_LONG).show();
                showSignInActivity();
            }

            dataLoadedEvent.emit(canteen);
        }
    }

    class UpdateCanteenTask extends AsyncTask<Canteen, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Canteen... params) {
            RemoteService service = RemoteServiceFactory.create(
                    CanteenManagerApplication.getInstance().getAuthToken());

            boolean result = service.updateCanteen(params[0]);

            setIsUnauthorized(service.isUnauthorized());

            return result;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (getIsUnauthorized()) {
                showSignInActivity();
            } else if (result) {
                _canteenNameText.setText(_canteen.name);
                _canteenWebSiteText.setText(_canteen.website);

                Toast.makeText(
                        getApplicationContext(), SAVED_SUCCESSFULLY, Toast.LENGTH_SHORT).show();

                loadFragment(CanteenOverviewFragment.class, true);
            } else {
                Toast.makeText(getApplicationContext(), CANNOT_SAVE, Toast.LENGTH_SHORT).show();
            }

            dataSavedEvent.emit();
        }
    }

    class NavigationItemSelectedListener implements NavigationView.OnNavigationItemSelectedListener {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Logger.getAnonymousLogger().info(item.toString());

            switch (item.getItemId()) {
                case R.id.manageCanteenOverview:
                    loadFragment(CanteenOverviewFragment.class, true);
                    break;

                case R.id.manageCanteenDetails:
                    loadFragment(CanteenDetailsFragment.class, true);
                    break;

                case R.id.manageCanteenRatings:
                    loadFragment(CanteenRatingsFragment.class, true);
                    break;

                case R.id.logout:
                    CanteenManagerApplication.getInstance().setAuthToken(null);
                    showSignInActivity();
                    break;

                case R.id.communicateShare:
                    Intent sendIntent = new Intent(Intent.ACTION_SEND);
                    sendIntent.setType("text/plain");
                    sendIntent.putExtra(Intent.EXTRA_TEXT,
                            String.format("Hey, check out my canteen: %s located at %s",
                                    getCanteen().name,
                                    getCanteen().address));
                    startActivity(Intent.createChooser(sendIntent, getString(R.string.send_to)));
                    break;
            }

            _drawerLayout.closeDrawers();

            return true;
        }
    }

    public synchronized boolean getIsUnauthorized() {
        return _isUnauthorized;
    }

    public synchronized Canteen getCanteen() {
        return _canteen;
    }

    public void loadData() {
        beginDataLoadEvent.emit();
        new GetCanteenTask().execute();
    }

    public void saveData() {
        beginDataSaveEvent.emit();
        new UpdateCanteenTask().execute(getCanteen());
    }

    public void showSignInActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivityForResult(intent, INTENT_REQUEST_LOGIN);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        _drawerLayout = findViewById(R.id.mainActivityDrawerLayout);

        _navigationView = findViewById(R.id.mainActivityNavigationView);
        _navigationView.setNavigationItemSelectedListener(
                new NavigationItemSelectedListener());

        _canteenNameText = _navigationView.getHeaderView(0).findViewById(R.id.canteenNameText);
        _canteenWebSiteText = _navigationView.getHeaderView(0).findViewById(R.id.canteenWebSiteText);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Create a button for the Drawer
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this,
                _drawerLayout,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        _drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        loadFragment(CanteenOverviewFragment.class, true);

        loadData();

        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver,
                EventBus.createCanteenChangedIntentFilter());
    }

    @Override
    public void onBackPressed() {
        if (!_drawerLayout.isDrawerOpen(Gravity.START))
            _drawerLayout.openDrawer(Gravity.START);
        else
            super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == INTENT_REQUEST_LOGIN && resultCode == RESULT_OK) {
            Logger.getLogger(TAG).info("onActivityResult");
            loadData();
        }
    }

    private void loadFragment(Class fragmentClass, boolean doReplace) {
        try {
            Fragment fragment = (Fragment) fragmentClass.newInstance();

            FragmentManager fragmentManager = getSupportFragmentManager();

            if (doReplace) {
                fragmentManager.beginTransaction()
                        .replace(R.id.mainActivityFragmentFrame, fragment, fragmentClass.toString())
                        .commit();
            } else {
                fragmentManager.beginTransaction()
                        .add(R.id.mainActivityFragmentFrame, fragment, fragmentClass.toString())
                        .commit();
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }

    private synchronized void setCanteen(Canteen canteen) {
        _canteen = canteen;
    }

    private synchronized void setIsUnauthorized(boolean val) {
        _isUnauthorized = val;
    }
}

