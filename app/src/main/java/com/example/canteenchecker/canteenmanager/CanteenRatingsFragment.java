package com.example.canteenchecker.canteenmanager;

import android.app.AlertDialog;
import android.app.Application;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.canteenchecker.canteenmanager.domain.Rating;
import com.example.canteenchecker.canteenmanager.remote.RemoteService;
import com.example.canteenchecker.canteenmanager.remote.RemoteServiceFactory;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Logger;

public class CanteenRatingsFragment extends Fragment {

    private static final String TAG = CanteenRatingsFragment.class.toString();

    class RemoveRatingTask extends AsyncTask<Rating, Void, Boolean> {
        RemoteService service;

        @Override
        protected Boolean doInBackground(Rating... ratings) {
            service = RemoteServiceFactory.create(
                    CanteenManagerApplication.getInstance().getAuthToken());

            return service.removeRating(ratings[0]);
        }

        @Override
        protected void onPostExecute(Boolean removed) {
            if (service.isUnauthorized())
                _mainActivity.showSignInActivity();
            else if (removed) {
                Toast.makeText(getContext(), R.string.rating_removed, Toast.LENGTH_SHORT).show();
                _mainActivity.loadData();
            } else {
                Toast.makeText(getContext(), R.string.rating_remove_failed, Toast.LENGTH_SHORT).show();
                _ratingAdapter.populate();
            }
        }
    }

    static class RatingAdapterViewHolder extends RecyclerView.ViewHolder {
        public TextView remarkTextView;
        public TextView authorTextView;
        public AppCompatRatingBar ratingBar;

        public RatingAdapterViewHolder(View itemView) {
            super(itemView);

            remarkTextView = itemView.findViewById(R.id.remark);
            authorTextView = itemView.findViewById(R.id.author);
            ratingBar = itemView.findViewById(R.id.rating);
        }
    }

    class RatingAdapter extends RecyclerView.Adapter<RatingAdapterViewHolder> {

        private ArrayList<Rating> _ratings = new ArrayList<>();

        public void populate() {
            _ratings.clear();

            if (_mainActivity.getCanteen() != null)
                _ratings.addAll(_mainActivity.getCanteen().ratings);

            notifyDataSetChanged();
        }

        public Rating remove(int position) {
            Rating rating = null;

            if (position < _ratings.size()) {
                rating = _ratings.get(position);

                _ratings.remove(position);
                notifyItemRemoved(position);

            }

            return rating;
        }

        @Override
        public RatingAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.ratings_listview_delegate, parent, false);

            return new RatingAdapterViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RatingAdapterViewHolder holder, int position) {
            if (position < _ratings.size()) {
                Rating rating = _ratings.get(position);

                if (rating != null) {
                    holder.remarkTextView.setText(rating.remark);
                    holder.authorTextView.setText(
                            String.format("%s \u00B7 %s",
                                    DateFormat.getDateFormat(getContext()).format(rating.timeStamp),
                                    rating.username));
                    holder.ratingBar.setRating(rating.ratingPoints);
                }
            }
        }

        @Override
        public long getItemId(int i) {
            return i < _ratings.size()? Long.parseLong(_ratings.get(i).ratingId): -1;
        }

        @Override
        public int getItemCount() {
            return _ratings.size();
        }
    }

    // Remove list elements by swiping away
    class RatingsTouchHelperCallback extends ItemTouchHelper.SimpleCallback {
        public RatingsTouchHelperCallback() {
            super(ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT,
                    ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
        }

        @Override
        public boolean onMove(RecyclerView recyclerView,
                              RecyclerView.ViewHolder viewHolder,
                              RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(final RecyclerView.ViewHolder viewHolder, int direction) {
            final Rating rating = _ratingAdapter.remove(viewHolder.getAdapterPosition());

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            builder.setMessage(R.string.rating_remove_confirm)
                    .setTitle(R.string.title_activity_main)
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            new RemoveRatingTask().execute(rating);
                        }
                    })
                    .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            _ratingAdapter.populate();
                        }
                    })
                    .show();
        }
    }

    private MainActivity _mainActivity;

    private RecyclerView _ratingsRecyclerView;
    private RatingAdapter _ratingAdapter;
    private SwipeRefreshLayout _layout;

    private Observer _uiEnabler = new Observer() {
        @Override
        public void update(Observable observable, Object o) {
            if (_layout != null && _ratingsRecyclerView != null) {
                _ratingAdapter.populate();
                _layout.setRefreshing(false);
                _ratingsRecyclerView.setEnabled(true);
            }
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        unsubscribe();
    }

    @Override
    public void onResume() {
        super.onResume();
        _mainActivity.getSupportActionBar().setTitle(R.string.title_fragment_ratings);
        subscribe();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _mainActivity = (MainActivity)getActivity();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_canteen_ratings, container, false);

        _ratingAdapter = new RatingAdapter();

        _ratingsRecyclerView = view.findViewById(R.id.ratingsRecyclerView);
        _ratingsRecyclerView.setHasFixedSize(true);
        _ratingsRecyclerView.setLayoutManager(new LinearLayoutManager(container.getContext()));
        _ratingsRecyclerView.setAdapter(_ratingAdapter);

        new ItemTouchHelper(new RatingsTouchHelperCallback()).attachToRecyclerView(
                _ratingsRecyclerView);

        _layout = view.findViewById(R.id.ratingsLayout);
        _layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                                         @Override
                                         public void onRefresh() {
                                             _layout.setRefreshing(true);
                                             _ratingsRecyclerView.setEnabled(false);

                                             _mainActivity.loadData();
                                         }
                                     });

        _ratingAdapter.populate();

        return view;
    }

    private void subscribe() {
        _mainActivity.dataLoadedEvent.addObserver(_uiEnabler);
    }

    private void unsubscribe() {
        _mainActivity.dataLoadedEvent.deleteObserver(_uiEnabler);
    }
}
