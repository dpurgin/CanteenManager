package com.example.canteenchecker.canteenmanager;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.canteenchecker.canteenmanager.remote.RemoteService;
import com.example.canteenchecker.canteenmanager.remote.RemoteServiceFactory;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Logger;

public class LoginActivity extends AppCompatActivity
{
    private final static String TAG = LoginActivity.class.toString();
    private final static String USERNAME_KEY = "userName";
    private final static String USERNAME_ERROR = "User name is required!";
    private final static String PASSWORD_ERROR = "Password is required!";
    private final static String USERNAME_PASSWORD_INVALID = "User name and/or password are invalid";
    private final static String UNEXPECTED_ERROR = "Unexpected error has occurred. Please try again later";

    private TextInputLayout userNameWrapper;
    private TextInputLayout passwordWrapper;
    private Button loginButton;

    private Date lastBackClick = Calendar.getInstance().getTime();

    class LoginTaskParams {
        public String userName;
        public String password;
    }

    class LoginTask extends AsyncTask<LoginTaskParams, Void, String> {
        boolean unexpectedError = false;

        @Override
        protected String doInBackground(LoginTaskParams... params) {
            RemoteService service = RemoteServiceFactory.create();

            String token = service.authenticate(params[0].userName, params[0].password);

            unexpectedError = (!service.isUnauthorized() && token == null);

            return token;
        }

        @Override
        protected void onPostExecute(String authToken) {
            // Unauthorized
            if (authToken == null || authToken.isEmpty()) {
                Toast.makeText(getApplicationContext(),
                               USERNAME_PASSWORD_INVALID,
                               Toast.LENGTH_SHORT).show();
                passwordWrapper.getEditText().setText(null);
            // Exception occurred (such as Internal server error)
            } else if (unexpectedError) {
                Toast.makeText(getApplicationContext(),
                               UNEXPECTED_ERROR,
                               Toast.LENGTH_SHORT).show();
            // OK -- auth token retrieved
            } else {
                CanteenManagerApplication.getInstance().setAuthToken(authToken);

                setResult(RESULT_OK);
                finish();
            }

            enableUi(true);
        }
    }

    class LoginButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            LoginTaskParams params = new LoginTaskParams();
            params.userName = userNameWrapper.getEditText().getText().toString();
            params.password = passwordWrapper.getEditText().getText().toString();

            userNameWrapper.setErrorEnabled(false);
            passwordWrapper.setErrorEnabled(false);

            if (params.userName.isEmpty()) {
                userNameWrapper.setError(USERNAME_ERROR);
            }

            if (params.password.isEmpty()) {
                passwordWrapper.setError(PASSWORD_ERROR);
            }

            // No errors -> make HTTP request
            if (userNameWrapper.getError() == null && passwordWrapper.getError() == null) {
                enableUi(false);
                new LoginTask().execute(params);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        userNameWrapper = findViewById(R.id.userNameWrapper);
        passwordWrapper = findViewById(R.id.passwordWrapper);
        loginButton = findViewById(R.id.loginButton);

        loginButton.setOnClickListener(new LoginButtonClickListener());
    }

    @Override
    public void onBackPressed() {
        if (Calendar.getInstance().getTime().getTime() - lastBackClick.getTime() > 2000) {
            Toast.makeText(getApplicationContext(), R.string.press_again, Toast.LENGTH_SHORT).show();
            lastBackClick = Calendar.getInstance().getTime();
        } else {
            finishAffinity();
        }
    }

    private void enableUi(boolean enable) {
        userNameWrapper.setEnabled(enable);
        passwordWrapper.setEnabled(enable);
        loginButton.setEnabled(enable);
    }
}
