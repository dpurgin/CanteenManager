package com.example.canteenchecker.canteenmanager;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatRatingBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.canteenchecker.canteenmanager.domain.Canteen;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Logger;


public class CanteenOverviewFragment extends Fragment {

    private static final String TAG = CanteenOverviewFragment.class.toString();
    private static final float DEFAULT_MAP_ZOOM_FACTOR = 15.0f;
    private MainActivity _mainActivity;

    private View viwProgress;
    private View viwContent;

    private TextView txvName;
    private TextView txvLocation;
    private TextView txvSetMeal;
    private TextView txvSetMealPrice;
    private TextView txvAverageWaitingTime;
    private ProgressBar prbAverageWaitingTime;
    private TextView reviewCountTextView;
    private AppCompatRatingBar ratingBar;

    private SupportMapFragment mpfMap;

    private Observer uiUpdater = new Observer() {
        @Override
        public void update(Observable observable, Object o) {
            updateUi();
        }
    };



    class GeocodeTask extends AsyncTask<Canteen, Void, LatLng> {
        @Override
        protected LatLng doInBackground(Canteen... params) {
            LatLng location = null;
            Geocoder geocoder = new Geocoder(getContext());
            try {
                List<Address> addresses = geocoder.getFromLocationName(params[0].address, 1);

                if (addresses != null && addresses.size() > 0) {
                    Address address = addresses.get(0);
                    location = new LatLng(address.getLatitude(), address.getLongitude());
                } else {
                    Logger.getLogger(TAG).warning("Resolving failed - no address received");
                }
            } catch(IOException e) {
                Logger.getLogger(TAG).warning("Resolving location failed");
            }

            return location;
        }

        @Override
        protected void onPostExecute(final LatLng latLng) {
            mpfMap.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    googleMap.clear();
                    if (latLng != null) {
                        googleMap.addMarker(new MarkerOptions().position(latLng));
                        googleMap.animateCamera(
                                CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_MAP_ZOOM_FACTOR));
                    } else {
                        googleMap.animateCamera(
                                CameraUpdateFactory.newLatLngZoom(new LatLng(0, 0), 0));
                    }
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        _mainActivity.getSupportActionBar().setTitle(R.string.title_fragment_overview);
        subscribe();
    }

    @Override
    public void onPause() {
        super.onPause();
        unsubscribe();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _mainActivity = (MainActivity)getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_canteen_overview, container, false);

        viwProgress = view.findViewById(R.id.viwProgress);
        viwContent = view.findViewById(R.id.viwContent);

        txvName = (TextView) view.findViewById(R.id.txvName);
        txvLocation = (TextView) view.findViewById(R.id.txvLocation);
        txvSetMeal = (TextView) view.findViewById(R.id.txvSetMeal);
        txvSetMealPrice = (TextView) view.findViewById(R.id.txvSetMealPrice);
        txvAverageWaitingTime = (TextView) view.findViewById(R.id.txvAverageWaitingTime);
        prbAverageWaitingTime = (ProgressBar) view.findViewById(R.id.prbAverageWaitingTime);

        reviewCountTextView = view.findViewById(R.id.reviewCountTextView);
        ratingBar = view.findViewById(R.id.ratingBar);

        mpfMap = (SupportMapFragment)getChildFragmentManager().findFragmentById(R.id.mpfMap);
        mpfMap.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                UiSettings uiSettings = googleMap.getUiSettings();
                uiSettings.setAllGesturesEnabled(false);
                uiSettings.setZoomControlsEnabled(true);
            }
        });

        updateUi();

        return view;
    }

    private void updateUi() {
        Canteen canteen = _mainActivity.getCanteen();

        if (canteen != null) {
            txvName.setText(canteen.name);
            txvLocation.setText(canteen.address);
            txvSetMeal.setText(canteen.meal);
            txvSetMealPrice.setText(NumberFormat.getCurrencyInstance().format(canteen.mealPrice));
            txvAverageWaitingTime.setText(canteen.averageWaitingTime);
            prbAverageWaitingTime.setProgress(Integer.parseInt(canteen.averageWaitingTime));

            reviewCountTextView.setText(String.valueOf(canteen.ratings.size()));
            ratingBar.setRating(Float.parseFloat(canteen.averageRating));

            viwProgress.setVisibility(View.GONE);
            viwContent.setVisibility(View.VISIBLE);

            new GeocodeTask().execute(canteen);
        }

    }

    private void subscribe() {
        _mainActivity.dataLoadedEvent.addObserver(uiUpdater);
        _mainActivity.dataSavedEvent.addObserver(uiUpdater);

    }

    private void unsubscribe() {
        _mainActivity.dataLoadedEvent.deleteObserver(uiUpdater);
        _mainActivity.dataSavedEvent.deleteObserver(uiUpdater);
    }

}
