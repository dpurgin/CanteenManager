package com.example.canteenchecker.canteenmanager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.canteenchecker.canteenmanager.domain.Canteen;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {
    private static final String TAG = MapsActivity.class.toString();
    private static final float DEFAULT_MAP_ZOOM_FACTOR = 12.0f;

    private GoogleMap mMap;
    private Menu _optionsMenu;
    private String _address;

    class GeocodeTask extends AsyncTask<String, Void, LatLng> {
        @Override
        protected LatLng doInBackground(String... params) {
            LatLng location = null;
            Geocoder geocoder = new Geocoder(getApplicationContext());
            try {
                List<Address> addresses = geocoder.getFromLocationName(params[0], 1);

                if (addresses != null && addresses.size() > 0) {
                    Address address = addresses.get(0);
                    location = new LatLng(address.getLatitude(), address.getLongitude());
                } else {
                    Logger.getLogger(TAG).warning("Resolving failed - no address received");
                }
            } catch(IOException e) {
                Logger.getLogger(TAG).warning("Resolving location failed");
            }

            return location;
        }

        @Override
        protected void onPostExecute(final LatLng latLng) {
            mMap.clear();
            if (latLng != null) {
                mMap.addMarker(new MarkerOptions().position(latLng));
                mMap.animateCamera(
                        CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_MAP_ZOOM_FACTOR));
            } else {
                mMap.animateCamera(
                        CameraUpdateFactory.newLatLngZoom(new LatLng(0, 0), 0));
            }
        }
    }

    class ReverseGeocodeTask extends AsyncTask<LatLng, Void, String> {
        @Override
        protected String doInBackground(LatLng... latLngs) {
            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());

            LatLng latLng = latLngs[0];

            StringBuilder sb = new StringBuilder();

            try {
                List<Address> addresses = geocoder.getFromLocation(
                        latLng.latitude, latLng.longitude, 1);

                if (addresses != null && addresses.size() > 0) {
                    Address address = addresses.get(0);

                    for (int i = 0; i <= address.getMaxAddressLineIndex(); ++i) {
                        if (sb.length() > 0)
                            sb.append(' ');

                        sb.append(address.getAddressLine(i));
                    }
                } else {
                    Logger.getLogger(TAG).warning("Resolving failed - no address received");
                }
            } catch (IOException e) {
                Logger.getLogger(TAG).warning("Resolving address failed");
            }

            return sb.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            Logger.getLogger(TAG).warning(s);

            if (!s.isEmpty()) {
                _optionsMenu.getItem(0).setEnabled(true);
                _address = s;
                Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(
                        getApplicationContext(),
                        R.string.address_resolution_failure,
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(R.string.title_activity_maps);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        _address = getIntent().getStringExtra("address");

        if (_address != null)
            new GeocodeTask().execute(_address);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_maps, menu);

        _optionsMenu = menu;

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.pickMenuItem) {
            Intent intent = new Intent();
            intent.putExtra("address", _address);
            setResult(Activity.RESULT_OK, intent);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                mMap.clear();
                mMap.addMarker(new MarkerOptions().position(latLng));

                _address = "";
                _optionsMenu.getItem(0).setEnabled(false);
                new ReverseGeocodeTask().execute(latLng);
            }
        });
    }
}
