package com.example.canteenchecker.canteenmanager;

import android.app.Application;

import com.google.firebase.FirebaseApp;
import com.google.firebase.messaging.FirebaseMessaging;

public class CanteenManagerApplication extends Application {
    private static final String CANTEENS_TOPIC_KEY = "canteens";
    private String authToken = null;

    private static CanteenManagerApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;

        FirebaseApp.initializeApp(instance);
        FirebaseMessaging.getInstance().subscribeToTopic(CANTEENS_TOPIC_KEY);
    }

    public static CanteenManagerApplication getInstance() {
        return instance;
    }

    public synchronized void setAuthToken(String token) {
        authToken = token;
    }

    public synchronized String getAuthToken() {
        return authToken;
    }

    public synchronized boolean isAuthorized() {
        return getAuthToken() != null && !getAuthToken().isEmpty();
    }
}
