package com.example.canteenchecker.canteenmanager.domain;

import java.util.Date;

public class Rating {
    public String ratingId;
    public String username;
    public String remark;
    public int ratingPoints;
    public Date timeStamp;
}
