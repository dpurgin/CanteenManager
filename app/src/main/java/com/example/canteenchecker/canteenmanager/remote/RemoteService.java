package com.example.canteenchecker.canteenmanager.remote;

import com.example.canteenchecker.canteenmanager.domain.Canteen;
import com.example.canteenchecker.canteenmanager.domain.Rating;

public interface RemoteService {
    String authenticate(String userName, String password);
    Canteen getCanteen();
    boolean updateCanteen(Canteen param);
    boolean removeRating(Rating rating);

    boolean isUnauthorized();
}
