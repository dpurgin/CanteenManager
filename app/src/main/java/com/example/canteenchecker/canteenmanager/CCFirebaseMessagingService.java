package com.example.canteenchecker.canteenmanager;

import android.support.v4.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

/**
 * Created by Dmitriy Purgin on 2017-11-25.
 */

public class CCFirebaseMessagingService extends FirebaseMessagingService {
    private static final String CANTEEN_DATA_CHANGED_KEY = "canteenDataChanged";
    private static final String TYPE_IDENTIFIER = "type";
    private static final String CANTEEN_ID_KEY = "canteenId";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Map<String, String> data = remoteMessage.getData();

        if (CANTEEN_DATA_CHANGED_KEY.equals(data.get(TYPE_IDENTIFIER))) {
            LocalBroadcastManager.getInstance(this).sendBroadcast(
                    EventBus.createCanteenChangedIntent(data.get(CANTEEN_ID_KEY)));
        }
    }
}
