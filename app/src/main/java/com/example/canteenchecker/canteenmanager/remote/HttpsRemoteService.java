package com.example.canteenchecker.canteenmanager.remote;

import com.example.canteenchecker.canteenmanager.domain.Canteen;
import com.example.canteenchecker.canteenmanager.domain.Rating;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Logger;

import javax.net.ssl.HttpsURLConnection;

public class HttpsRemoteService implements RemoteService {
    private static final String SERVICE_BASE_URL = "https://canteenchecker.azurewebsites.net/";
    private static final String TAG = HttpsRemoteService.class.toString();

    private String authToken = null;
    private int lastResponseCode = 0;
    private String lastResponseMessage = null;

    HttpsRemoteService(String authToken) {
        this.authToken = authToken;
    }

    @Override
    public String authenticate(String userName, String password) {
        String token = null;

        try {
            JSONObject object = new JSONObject();
            object.put("username", userName);
            object.put("password", password);

            token = post("Admin/Login", object);

            if (token != null)
                token = token.replaceAll("\"", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return token;
    }

    @Override
    public Canteen getCanteen() {
        Canteen result = null;

        try {
            JSONObject object = get("Admin/Canteen");

            if (getLastResponseCode() == HttpURLConnection.HTTP_OK) {
                result = new Canteen();

                result.canteenId = object.getString("canteenId");
                result.name = object.getString("name");
                result.meal = object.getString("meal");
                result.mealPrice = object.getDouble("mealPrice");
                result.address = object.getString("address");
                result.website = object.getString("website");
                result.phone = object.getString("phone");
                result.averageRating = object.getString("averageRating");
                result.averageWaitingTime = object.getString("averageWaitingTime");

                JSONArray ratingsArray = object.getJSONArray("ratings");

                for (int i = 0; i < ratingsArray.length(); ++i) {
                    JSONObject ratingsObject = ratingsArray.getJSONObject(i);

                    Rating rating = new Rating();
                    rating.ratingId = ratingsObject.getString("ratingId");
                    rating.username = ratingsObject.getString("username");
                    rating.remark = ratingsObject.getString("remark");
                    rating.ratingPoints = ratingsObject.getInt("ratingPoints");
                    rating.timeStamp = new Date(ratingsObject.getLong("timestamp"));

                    result.ratings.add(rating);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean updateCanteen(Canteen canteen) {
        if (canteen == null)
            return false;

        try {
            JSONObject object = new JSONObject();
            object.put("canteenId", canteen.canteenId);
            object.put("name", canteen.name);
            object.put("meal", canteen.meal);
            object.put("mealPrice", canteen.mealPrice);
            object.put("address", canteen.address);
            object.put("website", canteen.website);
            object.put("phone", canteen.phone);
            object.put("averageRating", canteen.averageRating);
            object.put("averageWaitingTime", canteen.averageWaitingTime);

            put("/Admin/Canteen", object);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return getLastResponseCode() == HttpsURLConnection.HTTP_OK;
    }

    @Override
    public boolean removeRating(Rating rating) {
        return delete("/Admin/Canteen/Rating/" + rating.ratingId);
    }

    @Override
    public boolean isUnauthorized() {
        return getLastResponseCode() == HttpURLConnection.HTTP_UNAUTHORIZED;
    }


    private HttpURLConnection createConnection(String service) throws IOException {
        Logger.getLogger(TAG).info(
                String.format(Locale.getDefault(), "Requesting %s", service));

        URL url = new URL(String.format("%s/%s", SERVICE_BASE_URL, service));
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();

        if (authToken != null) {
            connection.setRequestProperty("Authorization", String.format("Bearer %s", authToken));
        }

        return connection;
    }

    private boolean delete(String service) {
        boolean result = false;

        setLastResponseCode(0);
        setLastResponseMessage(null);

        HttpURLConnection connection = null;

        try {
            connection = createConnection(service);
            connection.setRequestMethod("DELETE");

            connection.connect();

            setLastResponseCode(connection.getResponseCode());
            setLastResponseMessage(connection.getResponseMessage());

            if (getLastResponseCode() == HttpURLConnection.HTTP_OK) {
                result = true;
            } else {
                String httpErrorText =
                        String.format("%d %s",
                                connection.getResponseCode(), connection.getResponseMessage());

                Logger.getLogger(TAG).warning(httpErrorText);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;

    }

    private JSONObject get(String service) throws JSONException {
        setLastResponseCode(0);
        setLastResponseMessage(null);

        HttpURLConnection connection = null;
        StringBuilder sb = new StringBuilder();
        JSONObject result = null;

        try {
            connection = createConnection(service);
            connection.setRequestMethod("GET");
            connection.setDoInput(true);

            connection.connect();

            setLastResponseCode(connection.getResponseCode());
            setLastResponseMessage(connection.getResponseMessage());

            if (getLastResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader inputStream =
                        new BufferedReader(new InputStreamReader(connection.getInputStream()));

                String str;
                while ((str = inputStream.readLine()) != null)
                    sb.append(str);

                result = new JSONObject(sb.toString());
            } else {
                String httpErrorText =
                        String.format("%d %s",
                                connection.getResponseCode(), connection.getResponseMessage());

                Logger.getLogger(TAG).warning(httpErrorText);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    private String post(String service, JSONObject request) {
        return postOrPut("POST", service, request);
    }

    private String put(String service, JSONObject request) {
        return postOrPut("PUT", service, request);
    }

    private String postOrPut(String verb, String service, JSONObject request) {
        setLastResponseCode(0);
        setLastResponseMessage(null);

        HttpURLConnection connection = null;
        StringBuilder sb = new StringBuilder();

        try {
            connection = createConnection(service);
            connection.setRequestMethod(verb);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoInput(true);

            OutputStream outputStream = connection.getOutputStream();
            outputStream.write(request.toString().getBytes("UTF-8"));
            outputStream.close();

            connection.connect();

            setLastResponseCode(connection.getResponseCode());
            setLastResponseMessage(connection.getResponseMessage());

            if (getLastResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader inputStream =
                        new BufferedReader(new InputStreamReader(connection.getInputStream()));

                String str;
                while ((str = inputStream.readLine()) != null)
                    sb.append(str);
            } else {
                String httpErrorText =
                        String.format("%d %s",
                                connection.getResponseCode(), connection.getResponseMessage());

                Logger.getLogger(TAG).warning(httpErrorText);
            }
        } catch (IOException e) {
            if (connection != null)
                connection.disconnect();
        }

        return sb.toString();
    }

    private int getLastResponseCode() {
        return lastResponseCode;
    }

    private String getLastResponseMessage() {
        return lastResponseMessage;
    }

    private void setLastResponseCode(int val) {
        lastResponseCode = val;
    }

    private void setLastResponseMessage(String val) {
        lastResponseMessage = val;
    }
}
