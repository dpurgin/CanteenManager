package com.example.canteenchecker.canteenmanager;

import android.content.Intent;
import android.content.IntentFilter;

public class EventBus {
    private static final String CANTEEN_CHANGED_ACTION = "EB_CanteenChanged";
    private static final String CANTEEN_ID_KEY = "CanteedId";

    public static Intent createCanteenChangedIntent(String canteenId) {
        Intent intent = new Intent(CANTEEN_CHANGED_ACTION);

        if (canteenId != null) {
            intent.putExtra(CANTEEN_ID_KEY, canteenId);
        }

        return intent;
    }

    public static IntentFilter createCanteenChangedIntentFilter() {
        return new IntentFilter(CANTEEN_CHANGED_ACTION);
    }

    public static String extractCanteenId(Intent intent) {
        return intent.getStringExtra(CANTEEN_ID_KEY);
    }

    private EventBus() {}
}
