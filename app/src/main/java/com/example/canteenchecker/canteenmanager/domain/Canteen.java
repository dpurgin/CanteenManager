package com.example.canteenchecker.canteenmanager.domain;

import java.util.ArrayList;

public class Canteen {
    public String canteenId;
    public String name;
    public String meal;
    public double mealPrice;
    public String address;
    public String website;
    public String phone;
    public String averageRating;
    public String averageWaitingTime;

    public ArrayList<Rating> ratings = new ArrayList<>();
}
